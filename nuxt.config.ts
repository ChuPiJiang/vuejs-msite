// import axios from "axios"

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    baseURL: '/',
    head: {
      htmlAttrs: {
        lang: 'zh-CN'
      },
      viewport: 'width=device-width, initial-scale=1, viewport-fit=cover',
      link: [
        {
          rel: 'preconnect',
          href: '//bedimage.jsvue.cn',
          crossorigin: true
        }
      ]
    }
  },
  components: [
    {
      path: '~/components/',
      pathPrefix: false
    }
  ],
  experimental: {
    externalVue: true,
    payloadExtraction: false
  },
  modules: [
    '@pinia/nuxt',
    '@vant/nuxt',
    'dayjs-nuxt',
    'nuxt-simple-sitemap'
  ],
  vant: {},
  site: {
    url: 'https://sfc.jsvue.cn',
  },
  // ssr: false, // csr must be false | ssr:ssg npm run generate must be true
  build: {
    transpile: [],
  },
  typescript: {
    shim: false
  },
  css: [
    'assets/css/index.css'
  ],
  postcss: {
    plugins: {
      'postcss-px-to-viewport-8-plugin': {
        viewportWidth: 375 // 设计稿的宽度 vant UI be 375
      }
    }
  },
  runtimeConfig: {
    public: {},
    app: {
      'BASE_URL': 'https://api.jsvue.cn',
      'XSRF_HEADER': 'x-basement-token',
      'BUS_TYPE': 7, // 上传业务枚举
      'APP_ID': 'H5D9B674D',
      'APP_NAME': '代码演示库',
    }
  },
  hooks: {
    async 'nitro:config'(nitroConfig:any) {
      if (nitroConfig.dev) return

      // const res = await axios.get('')
      const res = [4, 5, 6, 11, 22, 33, 45, 56, 67]
      nitroConfig.prerender.crawlLinks = true
      nitroConfig.prerender.routes = res.map(r => `/news/${r}`)
      return
    }
  },
  devServer: {
    host: 'test.jsvue.cn',
    https: true,
    port: 443
  }
})
