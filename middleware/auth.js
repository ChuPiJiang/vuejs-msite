import Cookies from 'js-cookie'
import qs from 'qs'
import { Base64 } from 'js-base64'

export default defineNuxtRouteMiddleware(async (to, from) => {
  const $config = useRuntimeConfig()
  const nuxtApp = useNuxtApp()
  // console.log(process.client)
  if (process.server) return
  const isLogin = Cookies.get('x-basement-token') && Cookies.get('OP_ID')
  if (!isLogin) {
    // console.log(from.path, to.path)
    return navigateTo('/login')
    // const fromUrl = window.location.origin + from.path
    // const params = qs.stringify({ appid: $config.app['APP_ID'], title: $config.app['APP_NAME'], fromUrl })
    // const state = Base64.encode(params, true)
    // window.location.href = `https://admin.jsvue.cn/html/m-login.html?state=${state}`
  }
})
