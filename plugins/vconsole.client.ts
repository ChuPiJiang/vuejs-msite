import VConsole from "vconsole"

export default defineNuxtPlugin(nuxtApp => {
  // 生产不显示
  if (process.env.NODE_ENV !== 'production') new VConsole()
})
