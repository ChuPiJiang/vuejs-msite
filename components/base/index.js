import MPage from './m-page'
import MNavBar from './m-navbar'
import MPanel from './m-panel'
import MTopnav from './m-topnav'
import MMessage from './m-message'
import MSelect from './m-select'
import MSteps from './m-steps'
import MPicker from './m-picker'
import MUpload from './m-upload'
import MAuthcode from './m-authcode'

export default {
  install(Vue) {
    Vue.component('MPage', MPage)
    Vue.component('MNavbar', MNavBar)
    Vue.component('MPanel', MPanel)
    Vue.component('MTopnav', MTopnav)
    Vue.component('MMessage', MMessage)
    Vue.component('MSelect', MSelect)
    Vue.component('MSteps', MSteps)
    Vue.component('MPicker', MPicker)
    Vue.component('MUpload', MUpload)
    Vue.component('MAuthcode', MAuthcode)
  },
}
