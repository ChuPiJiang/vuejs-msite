import qs from 'qs'
import { Base64 } from 'js-base64'

export function isWeChat() {
  const UA = process.server === false && window.navigator.userAgent.toLowerCase()
  return UA && (/micromessenger/gi).test(UA)
}

export function isDingTalk() {
  const UA = process.server === false && window.navigator.userAgent.toLowerCase()
  return UA && (/dingtalk/gi).test(UA)
}

export function isQQ() {
  const UA = process.server === false && window.navigator.userAgent.toLowerCase()
  return UA && (/mqqbrowser/gi).test(UA)
}
// |feishu||bytedance|weibo|

// 跳登录 fromUrl eg. encodeURIComponent(`${window.location.origin}/msite/list`)
export function goLogin(fromUrl) {
  const $config = useRuntimeConfig()
  const params = qs.stringify({ appid: $config.app['APP_ID'], title: $config.app['APP_NAME'], fromUrl })
  const state = Base64.encode(params, true)
  window.location.href = `https://admin.jsvue.cn/html/m-login.html?state=${state}`
}
