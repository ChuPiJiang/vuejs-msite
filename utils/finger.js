import Fingerprint2 from '@fingerprintjs/fingerprintjs'

// 获取浏览器指纹
export default function getFingerPrint () {
  return new Promise(resolve => {
    if (process.server === false && window.requestIdleCallback) {
      requestIdleCallback(() => {
        Fingerprint2.get(result => {
          const value = result.map(component => component.value)
          const visitorId = Fingerprint2.x64hash128(value.join(''), 31)
          resolve(visitorId)
        })
      })
    } else {
      setTimeout(() => {
        Fingerprint2.get(result => {
          const value = result.map(component => component.value)
          const visitorId = Fingerprint2.x64hash128(value.join(''), 31)
          resolve(visitorId)
        })
      }, 500)
    }
  })
}
