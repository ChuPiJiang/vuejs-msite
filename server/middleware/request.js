import getFingerPrint from '@/utils/finger'
import { getHeaders } from 'h3'

export default defineEventHandler(async (event) => {
  const reqHeaders = getHeaders(event)
  const ssrHeader = new Headers()
  const { app } = useRuntimeConfig()
  const clientId = await getFingerPrint()
  ssrHeader.set('cookie', reqHeaders.cookie)
  ssrHeader.set('x-xsrf-token', app['XSRF_HEADER'])
  ssrHeader.set('app-id', app['APP_ID'])
  ssrHeader.set('client-id', clientId)
  event.context.headers = ssrHeader
  event.context.baseUrl = app['BASE_URL']
})
