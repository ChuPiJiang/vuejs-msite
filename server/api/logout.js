import { readRawBody, getQuery, getMethod } from 'h3'

export default defineEventHandler(async (event) => {
  const method = getMethod(event).toUpperCase()
  let body
  if (method !== 'GET') body = await readRawBody(event)
  const res = await $fetch('/user-center/logout', {
    method,
    baseURL: event.context.baseUrl,
    headers: event.context.headers,
    params: getQuery(event),
    body
  })
  return res || {}
})
