import { defineStore } from 'pinia'

export const userStore = defineStore('app', {
  state() {
    return {
      x: 90
    }
  },
  persist: {
    enabled: true
  }
})
