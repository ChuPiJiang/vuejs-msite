import { defineStore } from 'pinia'

export const useUserStore = defineStore('userInfo', {
  state() {
    const userInfo = reactive({
      name: 'xxx'
    })
    return {
      userInfo
    }
  },
  persist: {
    enabled: true,
    strategies: [
      {
        key: 'user',
        storage: process.server === false && sessionStorage
      }
    ]
  }
})
