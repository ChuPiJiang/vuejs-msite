import Cookies from 'js-cookie'
import Http from '@/api/request'
import getFingerPrint from '@/utils/finger'

export const logout = () => {
  return Http.get('/user-center/logout')
}

export const destroyUser = () => {
  return Http.get('/user-center/user/destroyUser')
}

export const getUserInfo = (data) => {
  return Http.post('/user-center/user/getUserInfo', data)
}

export const setUserInviteCode = () => {
  return Http.get('/user-center/user/setUserInviteCode')
}

export const updateUser = (data) => {
  return Http.post('/user-center/user/updateUser', data)
}

export const setAvatar = async (formData) => {
  const file = formData.get('file')
  const $config = useRuntimeConfig()
  const businessType = $config.app['BUS_TYPE']
  const uploadToken = await Http.post('/user-center/watt/preUploadToken', {
    businessType,
    fileKey: file.name
  })
  const { data } = uploadToken.value || { data: {} }
  if (!data) {
    console.error('preUploadToken error!')
    return
  }
  const { key, token } = data
  formData.append('key', key)
  formData.append('token', token)
  formData.append('x:type', businessType)
  formData.append('x:deviceId', await getFingerPrint())
  formData.append('x:uid', Cookies.get('OP_ID'))
  await Http.reqData('https://upload-z2.qiniup.com/', formData)
  return Http.post('/user-center/setAvatar', { avatar: `https://static.jsvue.cn/${key}` })
}

export function getCaptcha (params) {
  return Http.get('/user-center/createCaptcha', params)
}

export function refreshCaptcha (params) {
  return Http.get('/user-center/refreshCaptcha', params)
}

export function sendSmsCode (data) {
  return Http.post('/user-center/sendSmsCode', data)
}

export function loginBySms (data) {
  return Http.post('/user-center/loginBySms', data)
}
